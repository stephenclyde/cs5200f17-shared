﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Utils;

namespace UtilsTesting
{
    [TestClass]
    public class SyncUtilsTest
    {
        private bool _flag;

        [TestMethod]
        public void SyncUtils_TestTimeout()
        {
            var ts = DateTime.Now;
            var result = SyncUtils.WaitForCondition(CheckFlag, 4000);
            var netTime = DateTime.Now.Subtract(ts).TotalMilliseconds;
            Assert.IsFalse(netTime < 4000);
            Assert.IsFalse(result);
        }


        [TestMethod]
        public void SyncUtils_TestConditionMetBeforeTimeout()
        {
            // Case 1 - Condition will be met before timeout
            _flag = false;
            var timer = new Timer(SetFlag, true, 1000, Timeout.Infinite);

            var ts = DateTime.Now;
            var result = SyncUtils.WaitForCondition(CheckFlag, 4000);
            var netTime = DateTime.Now.Subtract(ts).TotalMilliseconds;
            Assert.IsTrue(netTime < 4000);
            Assert.IsTrue(result);

            timer.Dispose();
        }

        [TestMethod]
        public void SyncUtils_TestTimeoutWithFalseCondition()
        {
            _flag = false;
            var timer = new Timer(SetFlag, true, 100, Timeout.Infinite);

            var ts = DateTime.Now;
            var result = SyncUtils.WaitForCondition(CheckFlag, 0);
            var netTime = DateTime.Now.Subtract(ts).TotalMilliseconds;
            Assert.IsTrue(netTime < 100);
            Assert.IsFalse(result);

            timer.Dispose();
        }

        [TestMethod]
        public void SyncUtils_TestZeroTimeoutWithTrueCondition()
        {
            _flag = true;
            var timer = new Timer(SetFlag, true, 100, Timeout.Infinite);

            var ts = DateTime.Now;
            var result = SyncUtils.WaitForCondition(CheckFlag, 0);
            var netTime = DateTime.Now.Subtract(ts).TotalMilliseconds;
            Assert.IsTrue(netTime < 100);
            Assert.IsTrue(result);

            timer.Dispose();
        }


        private void SetFlag(object state)
        {
            _flag = (bool)state;
        }

        private bool CheckFlag()
        {
            return _flag;
        }
    }
}
